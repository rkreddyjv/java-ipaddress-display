#!/bin/bash

#curl https://bitbucket.org/<username>/<repository>/get/<branchname>.zip -o <branchname>.zip
cd /home/ec2-user
wget https://bitbucket.org/rkreddyjv/java-ipaddress-display/raw/6d71b621449d7dc01a8e562b6284da172b2305e0/apache-tomcat-6.0.35.zip
unzip apache-tomcat-6.0.35.zip
cd /home/ec2-user/apache-tomcat-6.0.35/webapps
wget https://bitbucket.org/rkreddyjv/java-ipaddress-display/raw/66a0ef86825a154bcd377028667a581fcfbc9ef5/spring2.war
sudo chown -R ec2-user:ec2-user /home/ec2-user/apache-tomcat-6.0.35
cd /home/ec2-user/apache-tomcat-6.0.35/bin
chmod 755 *
./startup.sh
